package net.com.lorajose.agendaws.Objetos;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import net.com.lorajose.agendaws.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProcesosPHP implements
        Response.Listener<JSONObject>,Response.ErrorListener {
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Contactos> contactos = new ArrayList<Contactos>();
    private String serverip = "http://travelopolis.ddns.net/WebServices/";
    public void setContext(Context context){
        request = Volley.newRequestQueue(context);
    }
    public void insertarContactoWebService(Contactos c){
        String url = serverip + "wsRegistro.php?nombre="+c.getNombre()
                +"&telefono1="+c.getTelefono1()+"&telefono2=" +c.getTelefono2()+"&direccion="+c.getDireccion()
                +"&notas="+c.getNotas()+"&favorite="+c.getFavorite() +"&idMovil="+c.getIdMovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);

        /*Map<String, String> parametros = new HashMap<>();
        parametros.put("nombre",c.getNombre());
        parametros.put("telefono1",c.getTelefono1());
        parametros.put("telefono2",c.getTelefono2());
        parametros.put("direccion",c.getDireccion());
        parametros.put("notas",c.getNotas());
        parametros.put("favorite", String.valueOf(c.getFavorite()));
        parametros.put("idMovil",c.getIdMovil());

        /*JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, "http://travelopolis.ddns.net/WebServices/wsRegistro.php", new JSONObject(parametros), new Response.Listener<JSONObject>()
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,"http://travelopolis.ddns.net/WebServices/wsRegistro.php",new JSONObject(parametros),this,this);
        request.add(jsonObjectRequest);*/


    }
    public void actualizarContactoWebService(Contactos c,int id){
        String url = serverip + "wsActualizar.php?_ID="+id
                +"&nombre="+c.getNombre()+"&direccion="+c.getDireccion() +"&telefono1="+c.getTelefono1()+"&telefono2="+c.getTelefono2() +"&notas="+c.getNotas()+"&favorite="+c.getFavorite();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void borrarContactoWebService(int id){
        String url = serverip + "wsEliminar.php?_ID="+id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }
    @Override
    public void onResponse(JSONObject response) {
    }
}
