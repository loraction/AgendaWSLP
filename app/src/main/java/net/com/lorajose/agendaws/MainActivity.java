package net.com.lorajose.agendaws;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


import net.com.lorajose.agendaws.Objetos.Contactos;
import net.com.lorajose.agendaws.Objetos.Device;
import net.com.lorajose.agendaws.Objetos.ProcesosPHP;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener {
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private TextView txtNombre;
    private TextView txtDireccion;
    private TextView txtTelefono1;
    private TextView txtTelefono2;
    private TextView txtNotas;
    private CheckBox cbkFavorite;
    private Contactos savedContacto;

    private String txtNombreCompara;
    private String txtDireccionCompara;
    private String txtTelefono1Compara;
    private String txtTelefono2Compara;
    private String txtNotasCompara;
    private int cbkFavoritoCompara;

    ProcesosPHP php;
    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEvents();
    }
    public void initComponents() {
        this.php = new ProcesosPHP();
        php.setContext(this);
        this.txtNombre = findViewById(R.id.edtNombre);
        this.txtTelefono1 = findViewById(R.id.edtTelefono1);
        this.txtTelefono2 = findViewById(R.id.edtTelefono2);
        this.txtDireccion = findViewById(R.id.edtDireccion);
        this.txtNotas = findViewById(R.id.edtNotas);
        this.cbkFavorite = findViewById(R.id.cbxFavorito);
        this.btnGuardar = findViewById(R.id.btnGuardar);
        this.btnListar = findViewById(R.id.btnListar);
        this.btnLimpiar = findViewById(R.id.btnLimpiar);
        savedContacto = null;
    }
    public void setEvents() {
        this.btnGuardar.setOnClickListener(this);
        this.btnListar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if(isNetworkAvailable()){
            switch (view.getId()) {
                case R.id.btnGuardar:
                    boolean completo = true;
                    if(txtNombre.getText().toString().equals("")){
                        txtNombre.setError("Introduce el Nombre");
                        completo=false;
                    }
                    if(txtTelefono1.getText().toString().equals("")){
                        txtTelefono1.setError("Introduce el Telefono Principal");
                        completo=false;
                    }
                    if(txtDireccion.getText().toString().equals("")){
                        txtDireccion.setError("Introduce la Direccion");
                        completo=false;
                    }
                    if (completo){
                        final Contactos nContacto = new Contactos();
                        nContacto.setNombre(txtNombre.getText().toString());
                        nContacto.setTelefono1(txtTelefono1.getText().toString());
                        nContacto.setTelefono2(txtTelefono2.getText().toString());
                        nContacto.setDireccion(txtDireccion.getText().toString());
                        nContacto.setNotas(txtNotas.getText().toString());
                        nContacto.setFavorite(cbkFavorite.isChecked() ? 1 : 0);
                        int favorito = nContacto.getFavorite();
                        nContacto.setIdMovil(Device.getSecureId(this));
                        if(savedContacto == null){
                            php.insertarContactoWebService(nContacto);
                            Toast.makeText(getApplicationContext(),"Contacto Ingresado", Toast.LENGTH_SHORT).show();
                            limpiar();
                        }else{
                            if(txtNombreCompara.equals(txtNombre.getText().toString()) && txtTelefono1Compara.equals(txtTelefono1.getText().toString()) && txtTelefono2Compara.equals(txtTelefono2.getText().toString()) && txtDireccionCompara.equals(txtDireccion.getText().toString()) && txtNotasCompara.equals(txtNotas.getText().toString()) &&
                                    favorito == cbkFavoritoCompara ){
                                php.actualizarContactoWebService(nContacto,id);
                                Toast.makeText(getApplicationContext(),"Sin Cambios",Toast.LENGTH_SHORT).show();
                                limpiar();
                            }else{
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("Actualizar Contacto")
                                        .setMessage("¿Estas seguro de actualizar     este cntacto?")

                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                php.actualizarContactoWebService(nContacto,id);
                                                Toast.makeText(getApplicationContext(),"Contacto Actualizado",Toast.LENGTH_SHORT).show();
                                                limpiar();
                                            }
                                        })
                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Toast.makeText(getApplicationContext(), "Operacion Abortada", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }
                        }
                    }
                    break;
                case R.id.btnLimpiar:
                    limpiar();
                    break;
                case R.id.btnListar:
                    Intent i= new Intent(MainActivity.this,ListaActivity.class);
                    limpiar();
                    startActivityForResult(i,0);
                    break;
            }
        }else{
            Toast.makeText(getApplicationContext(), "Se necesita tener conexion a internet", Toast.LENGTH_SHORT).show();
        }
    }
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
    public void limpiar(){
        savedContacto = null;
        txtNombre.setText("");
        txtTelefono1.setText("");
        txtTelefono2.setText("");
        txtNotas.setText("");
        txtDireccion.setText("");
        cbkFavorite.setChecked(false);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){
            Bundle oBundle = intent.getExtras();
            if(Activity.RESULT_OK == resultCode){Contactos contacto = (Contactos) oBundle.getSerializable("contacto");
                savedContacto = contacto;
                id = contacto.get_ID();
                txtNombre.setText(contacto.getNombre());
                txtNombreCompara=contacto.getNombre();
                txtTelefono1.setText(contacto.getTelefono1());
                txtTelefono1Compara=contacto.getTelefono1();
                txtTelefono2.setText(contacto.getTelefono2());
                txtTelefono2Compara=contacto.getTelefono2();
                txtDireccion.setText(contacto.getDireccion());
                txtDireccionCompara = contacto.getDireccion();
                txtNotas.setText(contacto.getNotas());
                txtNotasCompara = contacto.getNotas();
                if(contacto.getFavorite()>0)
                {
                    cbkFavorite.setChecked(true);
                    cbkFavoritoCompara = 1;
                }
                else{
                    cbkFavoritoCompara = 0;
                }
            }else{
                limpiar();
            }
        }
    }
}
