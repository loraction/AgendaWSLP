package net.com.lorajose.agendaws;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import net.com.lorajose.agendaws.Objetos.Contactos;
import net.com.lorajose.agendaws.Objetos.Device;
import net.com.lorajose.agendaws.Objetos.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity  implements Response.Listener<JSONObject>,Response.ErrorListener{
    private Button btnNuevo;
    private final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private MyArrayAdapter adapter;
    private ArrayList<Contactos> listaContactos;
    private ListView listView;
    private String serverip = "http://travelopolis.ddns.net/WebServices/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        listaContactos = new ArrayList<>();
        request = Volley.newRequestQueue(context);
        consultarTodosWebService();
        listView = findViewById(R.id.list);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }
    public void consultarTodosWebService(){
        String url = serverip + "wsConsultarTodos.php?idMovil="+ Device.getSecureId(this);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onResponse(JSONObject response) {
        Contactos contacto = null;
        JSONArray json = response.optJSONArray("contactos");
        try {
            for(int i=0;i<json.length();i++){
                contacto = new Contactos();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                contacto.set_ID(jsonObject.optInt("_ID"));
                contacto.setNombre(jsonObject.optString("nombre"));
                contacto.setTelefono1(jsonObject.optString("telefono1"));
                contacto.setTelefono2(jsonObject.optString("telefono2"));
                contacto.setDireccion(jsonObject.optString("direccion"));
                contacto.setNotas(jsonObject.optString("notas"));
                contacto.setFavorite(jsonObject.optInt("favorite"));
                contacto.setIdMovil(jsonObject.optString("idMovil"));
                listaContactos.add(contacto);
            }
            adapter = new MyArrayAdapter(context,R.layout.layout_contacto,listaContactos);
            listView.setAdapter(adapter);
            //setListAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class MyArrayAdapter extends ArrayAdapter<Contactos> {
        Context context;
        int textViewRecursoId;
        ArrayList<Contactos> objects;
        ArrayList<Contactos> copyContactos;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Contactos> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
            copyContactos = new ArrayList<>();
            copyContactos.addAll(objects);
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = (TextView) view.findViewById(R.id.lblTelefonoContacto);
            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);
            if (position % 2 == 0) {
                view.setBackgroundColor(context.getResources().getColor(R.color.verde));
            }
            else {
                view.setBackgroundColor(context.getResources().getColor(R.color.azul));
            }
            if(objects.get(position).getFavorite()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }else{
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getTelefono1());
            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    new AlertDialog.Builder(context)
                            .setTitle("Eliminar Contacto")
                            .setMessage("¿Estas seguro de eliminar este cntacto?")

                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    php.setContext(context);
                                    Log.i("id", String.valueOf(objects.get(position).get_ID()));
                                    php.borrarContactoWebService(objects.get(position).get_ID());
                                    objects.remove(position);
                                    notifyDataSetChanged();
                                    Toast.makeText(getApplicationContext(), "Contacto eliminado con exito", Toast.LENGTH_SHORT).show();
                                }
                            })

                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getApplicationContext(), "Operacion Abortada", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            });
            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }

        public void filtrar(String s) {
            this.objects.clear();
            if (s.equals("")) {
                this.objects.addAll(copyContactos);
            }
            else {
                for (Contactos con : copyContactos) {
                    if (con.getNombre().toLowerCase().contains(s)) {
                        this.objects.add(con);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Asignar Menu
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filtrar(newText.toLowerCase());
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

}

